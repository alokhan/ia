'''NAMES OF THE AUTHOR(S): Florent Klein and Alois Paulus'''

from search import *
import time
from math import *



###################### Graph                 #######################
class Vertex:
    def __init__(self,key):
        self.id = key
        self.connectedTo = {}

    def addNeighbor(self,nbr,weight=0):
        self.connectedTo[nbr] = weight

    def __str__(self):
        return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo])

    def getConnections(self):
        return self.connectedTo.keys()

    def getId(self):
        return self.id

    def getWeight(self,nbr):
        return self.connectedTo[nbr]

    def getClosestVertex(self):
        distance = 9999999999999
        closest = None
        for x in self.connectedTo:
            if x != self:
                if (distance > self.getWeight(x)):
                    distance = self.getWeight(x)
                    closest = x
        return [closest,distance]

class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0

    def addVertex(self,key):
        self.numVertices = self.numVertices + 1
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self,n):
        if n in self.vertList:
            return self.vertList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList

    def addEdge(self,f,t,cost=0):
        if f not in self.vertList:
            nv = self.addVertex(f)
        if t not in self.vertList:
            nv = self.addVertex(t)
        self.vertList[f].addNeighbor(self.vertList[t], cost)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())




######################  Implement the search #######################

class Position:

    def __init__(self,i,u):
        self.x = i
        self.y = u
    def __str__(self):
        return "x: " + str(self.x) + " y: " + str(self.y)

    def __eq__(self,other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y
        else:
            return False

    def FindManathanDistance(self, other):
        xdiff = abs(self.x - other.x)
        ydiff = abs(self.y - other.y)
        return xdiff + ydiff

    def EuclideanDistance(self, other):
        xdiff = (self.x - other.x)**2
        ydiff = (self.y - other.y)**2
        return math.sqrt(xdiff + ydiff)


    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.x + self.y)

class Move:

    def __init__(self, kind, position):
        self.kind = kind
        self.nextPosition = position

# The class state with str redefined for specific output format
class State:

    def __init__(self,board, moneyPositions = [], mcDuckPosition = None, safePosition = None, graph = None):
        self.board = board
        if (mcDuckPosition == None):
            self.McDuckPosition = None
            self.SafePosition = None
            self.MoneyPositions = []
            self.GetInfoFromBoard()
        else:
            self.McDuckPosition = mcDuckPosition
            self.SafePosition = safePosition
            self.MoneyPositions = moneyPositions

        self.Graph = Graph()

        for money in self.MoneyPositions:
            distanceFromDuckToMoney = self.McDuckPosition.FindManathanDistance(money)
            distanceFromSafeToMoney = self.SafePosition.FindManathanDistance(money)
            self.Graph.addEdge(self.McDuckPosition,money,distanceFromDuckToMoney)
            self.Graph.addEdge(self.SafePosition,money,distanceFromSafeToMoney)
            for otherMoney in self.MoneyPositions:
                distanceFromMoneyToMoney = money.FindManathanDistance(otherMoney)
                self.Graph.addEdge(money,otherMoney,distanceFromMoneyToMoney)

        self.Graph.addEdge(self.McDuckPosition, self.SafePosition,self.McDuckPosition.FindManathanDistance(self.SafePosition))

    def __str__(self):
        output=""
        for i in range(0,len(self.board)):
            line=""
            for u in range(0,len(self.board[i])):
                if (u < len(self.board[i]) - 1):
                    line=line+self.board[i][u]
                else:
                    line=line+self.board[i][u]
            output= output + line + "\n"
        return output

    def __eq__(self,other):
        return hash(self) == hash(other)

    def __hash__(self):
        stateTuple = ()
        for pos in self.MoneyPositions:
            stateTuple = ((pos.x,pos.y),) + stateTuple
        stateTuple = stateTuple + (self.McDuckPosition.x,self.McDuckPosition.y) + (self.SafePosition.x,self.SafePosition.y)
        return hash(stateTuple)

    def GetInfoFromBoard(self):
        moneyPositions = []
        for i in range(0,len(self.board)):
           for u in range(0,len(self.board[i])):
               if(self.board[i][u] == "@"):
                   self.McDuckPosition = Position(i,u)
               elif (self.board[i][u] == "+"):
                   self.SafePosition = Position(i,u)
               elif(self.board[i][u] == "$"):
                   money = Position(i,u)
                   moneyPositions.append(money)
        self.MoneyPositions = moneyPositions

class Koutack(Problem):

    def __init__(self,init):
        self.numberNodeExplored = 0
        # Open file with path given as param
        f = open(init, 'r')
        # Read the each line of the file into a list
        lines = f.readlines()
        f.close()
        initBoard = ()
        # Construct a tuple of tuples for the state
        length = len(lines)
        width = len(lines[0])
        for i in range(len(lines) -1, -1 , -1):
            initBoard = (tuple(lines[i].replace("\n", "")),) + initBoard

        initState = State(initBoard)
        super().__init__(initState)

    # Check if only on tile is not a dot
    def goal_test(self, state):
        count = 0;
        self.numberNodeExplored += 1
        i = 0
        while i < len(state.board) and count == 0:
            count = count + state.board[i].count('$')
            i += 1
        return (count == 0 and (state.McDuckPosition == state.SafePosition))


    def successor(self, state):
        for move in self.WhereCanIMove(state):
            yield [move,self.getNextState(move,state)]

    def WhereCanIMove(self,state):
        moves = []
        i = state.McDuckPosition.x
        u = state.McDuckPosition.y
        if (i > 0) :
            cell = state.board[i-1][u]
            if ((cell != "#")):
                moves.append(Move("down",Position(i-1,u)))
        if (u + 1 < len(state.board[i])):
            cell = state.board[i][u+1]
            if ((cell != "#") ):
                moves.append(Move("right",Position(i,u+1)))
        if (i + 1 < len(state.board)):
            cell = state.board[i+1][u]
            if ((cell != "#")):
                    moves.append(Move("up",Position(i+1,u)))
        if (u > 0):
            cell = state.board[i][u-1]
            if ((cell != "#")):
                moves.append(Move("left",Position(i,u-1)))
        return moves

    # Merge all the neighbours from the current position to a tile
    # and return a new state
    def getNextState(self,move,state):

        safePosition = state.SafePosition
        mcDuckPosition = state.McDuckPosition
        moneyPositions = list(state.MoneyPositions)
        mcDuckNextPosition = move.nextPosition
        newStateList = []
        for tup in state.board:
            newStateList.append(list(tup))

        newStateList[mcDuckPosition.x][mcDuckPosition.y] = " "

        if (newStateList[mcDuckNextPosition.x][mcDuckNextPosition.y] == "$"):
             moneyPositions.remove(mcDuckNextPosition)

        newStateList[mcDuckNextPosition.x][mcDuckNextPosition.y] = "@"

        #to allow to going on the safe to collect money
        if (mcDuckNextPosition != safePosition):
             newStateList[safePosition.x][safePosition.y] = "+"

        newStateTuple = ()
        for i in range(len(newStateList)-1, -1 , -1):
            newStateTuple = (tuple(newStateList[i]),) + newStateTuple
        newState = State(newStateTuple,moneyPositions,mcDuckNextPosition,state.SafePosition,state.Graph)
        return newState

    def h(self, node):

        return 0

###################### Launch the search #########################
startTime = time.time()
problem=Koutack(sys.argv[1])
#example of bfs search
node=astar_graph_search(problem, problem.h)
#example of print
path=node.path()
path.reverse()
for n in path:
    print(n.state) #assuming that the __str__ function of states output the correct format

if (len(sys.argv) > 2 and sys.argv[2] == "info"):
    print(sys.argv[1].split("/")[2] + " & " + str(time.time() - startTime) + " & "+ str(problem.numberNodeExplored) + " & " + str(len(path)) + " \\\\ \hline ")

