'''NAMES OF THE AUTHOR(S): Florent Klein and Alois Paulus'''

from search import *
import time
from math import *
######################  Implement the search #######################

class Position:

    def __init__(self,i,u):
        self.x = i
        self.y = u
    def __str__(self):
        return "x: " + str(self.x) + " y: " + str(self.y)

    def __eq__(self,other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y
        else:
            return False

    def FindManathanDistance(self, other):
        xdiff = abs(self.x - other.x)
        ydiff = abs(self.y - other.y)
        return xdiff + ydiff

    def EuclideanDistance(self, other):
        xdiff = (self.x - other.x)**2
        ydiff = (self.y - other.y)**2
        return math.sqrt(xdiff + ydiff)


    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.x + self.y)

class Move:

    def __init__(self, kind, position):
        self.kind = kind
        self.nextPosition = position

# The class state with str redefined for specific output format
class State:

    def __init__(self,width, height,wallPositions = {}, moneyPositions = [], up=[], down=[] ,mcDuckPosition = None, safePosition = None):

        self.Width = width
        self.Height = height
        self.McDuckPosition = mcDuckPosition
        self.SafePosition = safePosition
        self.MoneyPositions = moneyPositions
        self.MoneyPositionsUp = up
        self.MoneyPositionsDown = down
        self.WallPositions = wallPositions

    def __str__(self):
        output=""
        for i in range(0,self.Height):
            line=""
            for u in range(0,self.Width):
                pos = Position(i,u)
                if (pos in self.WallPositions):
                    line+="#"
                elif (pos in self.MoneyPositions):
                    line+="$"
                elif (pos == self.McDuckPosition):
                    line+="@"
                elif (pos == self.SafePosition):
                    line+="+"
                else:
                    line+=" "
            output= output + line + "\n"
        return output

    def __eq__(self,other):
        return hash(self) == hash(other)

    def __hash__(self):
        stateTuple = ()
        for pos in self.MoneyPositions:
            stateTuple = ((pos.x,pos.y),) + stateTuple
        stateTuple = stateTuple + (self.McDuckPosition.x,self.McDuckPosition.y) + (self.SafePosition.x,self.SafePosition.y)
        return hash(stateTuple)

class Koutack(Problem):

    def __init__(self,init):
        self.numberNodeExplored = 0
        # Open file with path given as param
        f = open(init, 'r')
        # Read the each line of the file into a list
        lines = f.readlines()
        f.close()
        initBoard = ()
        # Construct a tuple of tuples for the state

        for i in range(len(lines) -1, -1 , -1):
            initBoard = (tuple(lines[i].replace("\n", "")),) + initBoard
 
        #Process the board to build state object
        height = len(initBoard)
        width = len(initBoard[0])
        moneyPositions = []
        wallPositions = {}
        for i in range(0,len(initBoard)):
           for u in range(0,len(initBoard[i])):
               if(initBoard[i][u] == "#"):
                   wall = Position(i,u)
                   wallPositions[wall] = True
               elif(initBoard[i][u] == "@"):
                   McDuckPosition = Position(i,u)
               elif (initBoard[i][u] == "+"):
                   SafePosition = Position(i,u)
               elif(initBoard[i][u] == "$"):
                   money = Position(i,u)
                   moneyPositions.append(money)

        up = []
        down = []
           
        for money in moneyPositions:
            if (money.y <= SafePosition.y):
                up.append(money)
            else:
                down.append(money)

        initState = State(width,height,wallPositions,moneyPositions,up,down,McDuckPosition,SafePosition)
        goalState = State(width,height,wallPositions,[],[],[],SafePosition,SafePosition)
        super().__init__(initState, goalState)

    # Check if only on tile is not a dot
    def goal_test(self, state):
        self.numberNodeExplored += 1
        return super().goal_test(state)


    def successor(self, state):
        for move in self.WhereCanIMove(state):
            yield [move,self.getNextState(move,state)]

    def WhereCanIMove(self,state):
        moves = []
        i = state.McDuckPosition.x
        u = state.McDuckPosition.y
        if (i > 0) :
            pos = Position(i-1,u)
            if (pos not in state.WallPositions):
                moves.append(Move("down",pos))
        if (u + 1 < state.Width):
            pos = Position(i,u+1)
            if (pos not in state.WallPositions):
                moves.append(Move("right",pos))
        if (i + 1 < state.Height):
            pos = Position(i+1,u)
            if (pos not in state.WallPositions):
                    moves.append(Move("up",pos))
        if (u > 0):
            pos = Position(i,u-1)
            if (pos not in state.WallPositions):
                moves.append(Move("left",pos))
        return moves

    # Merge all the neighbours from the current position to a tile
    # and return a new state
    def getNextState(self,move,state):

        safePosition = state.SafePosition
        mcDuckPosition = state.McDuckPosition
        moneyPositions = list(state.MoneyPositions)
        up = list(state.MoneyPositionsUp)
        down = list(state.MoneyPositionsDown)
        mcDuckNextPosition = move.nextPosition

        if (mcDuckNextPosition in moneyPositions):
             moneyPositions.remove(mcDuckNextPosition)

        if (mcDuckNextPosition in up):
             up.remove(mcDuckNextPosition)

        if (mcDuckNextPosition in down):
             down.remove(mcDuckNextPosition)

        newState = State(state.Width,state.Height,state.WallPositions,moneyPositions,up,down,mcDuckNextPosition,state.SafePosition)
        return newState

    def h(self, node):

        fartestMoney1DistanceFromSafe = 0
        fartestMoney1FromSafe = None
        fartestMoney2DistanceFromSafe = 0
        fartestMoney2FromSafe = None
        slicedMoneyPositions = []
        distanceDuckToFartestMoney1 = 9999999
        distanceDuckToFartestMoney2 = 9999999

        if node.state.SafePosition == None:
            return 0

        for money in node.state.MoneyPositions:
            distanceFromSafeToMoney = node.state.SafePosition.FindManathanDistance(money)
            if (money.y <= node.state.SafePosition.y):         
                if (fartestMoney1DistanceFromSafe < distanceFromSafeToMoney):
                    fartestMoney1DistanceFromSafe = distanceFromSafeToMoney
                    fartestMoney1FromSafe = money
            else:
                if (fartestMoney2DistanceFromSafe < distanceFromSafeToMoney):
                    fartestMoney2DistanceFromSafe = distanceFromSafeToMoney
                    fartestMoney2FromSafe = money

        if fartestMoney1FromSafe == None and fartestMoney2FromSafe == None:
            return node.state.McDuckPosition.FindManathanDistance(node.state.SafePosition)

        if (fartestMoney1FromSafe != None):
            distanceDuckToFartestMoney1 =  node.state.McDuckPosition.FindManathanDistance(fartestMoney1FromSafe)
            
        if (fartestMoney2FromSafe != None):
            distanceDuckToFartestMoney2 =  node.state.McDuckPosition.FindManathanDistance(fartestMoney2FromSafe)   

        if (distanceDuckToFartestMoney1 < distanceDuckToFartestMoney2):
            distanceDuckToClosestFartestMoney = distanceDuckToFartestMoney1
        else:
            distanceDuckToClosestFartestMoney = distanceDuckToFartestMoney2

        return distanceDuckToClosestFartestMoney + fartestMoney1DistanceFromSafe + fartestMoney2DistanceFromSafe

###################### Launch the search #########################
startTime = time.time()
problem=Koutack(sys.argv[1])
#example of bfs search
node=breadth_first_graph_search(problem,)
#example of print
path=node.path()
path.reverse()
if (len(sys.argv) > 2 and sys.argv[2] != "Onlyinfo"):
    for n in path:
        print(n.state) #assuming that the __str__ function of states output the correct format

if (len(sys.argv) > 2 and sys.argv[2] == "info" or len(sys.argv) > 2 and sys.argv[2] == "Onlyinfo"):
    print(sys.argv[1].split("/")[2] + " & " + str(time.time() - startTime) + " & "+ str(problem.numberNodeExplored) + " & " + str(len(path)) + " \\\\ \hline ")

