#!/usr/bin/env python3
"""
Zombies agent.
Copyright (C) 2014, <<<<<<<<<<< Florent Klein and Alois Paulus >>>>>>>>>>>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.

"""

import zombies
import minimax
import copy
import pdb
from random import randint

# The empty available (reachable) tile
EMPTY = 0
# The necromancer tile
NECROMANCER = 1
# The zombie huger tile
HUGGER = 2
# The zombie jumper tile
JUMPER = 3
# The zombie creeper tile
CREEPER = 4
# The zombie sprinter tile
SPRINTER = 5

class Successor:

      def __init__(self,emptyNb, action, state):
          self.emptyTile = emptyNb
          self.action = action
          self.state = state

      def __eq__(self,other):
          if isinstance(other, self.__class__):
              return self.emptyTile == other.emptyTile
          else:
              return False

      def __lt__(self,other):
          if isinstance(other, self.__class__):
              return self.emptyTile < other.emptyTile
          else:
              return False

      def __gt__(self,other):
          if isinstance(other, self.__class__):
              return self.emptyTile > other.emptyTile
          else:
              return False


class Agent(zombies.Agent, minimax.Game):
    """This is the skeleton of an agent to play the Zombies game."""

    def __init__(self, name="Advanced Agent"):
        self.name = name
        self.player = zombies.PLAYER1
        self.otherPlayer = zombies.PLAYER2
        self.lastPlayedMoves = []
        self.changeStrategy = False;

    def successors(self, state):
        """The successors function must return (or yield) a list of
        pairs (a, s) in which a is the action played to reach the
        state s; s is the new state, i.e. a triplet (b, p, st) where
        b is the new board after the action a has been played,
        p is the player to play the next move and st is the next
        step number.
        """
        board = state[0]
        player = state[1]
        stepNumber =  state[2]
        sortedSuccessors = []
        minEmptyTile = 6
        insertIndex = 1

        otherNecro = self.getNecromancerPiece(board, self.otherPlayer)
        myNecro = self.getNecromancerPiece(board, self.player)
        for action in board.get_actions(player, stepNumber):
            newStepNumber = stepNumber + 1
            newAction = tuple(action)
            newBoard = board.clone()
            newBoard = newBoard.play_action(action,player,stepNumber)

            if (player == zombies.PLAYER1):
                newPlayer = zombies.PLAYER2
            else:
                newPlayer = zombies.PLAYER1

            # if necro moved recalculate his position
            if otherNecro == None:
                otherNecro = self.getNecromancerPiece(newBoard, self.otherPlayer)
            if myNecro == None:
                myNecro = self.getNecromancerPiece(newBoard, self.player)
            if otherNecro != None and not (self.isPieceNecro(otherNecro,newBoard,self.otherPlayer)):
                otherNecro = self.getNecromancerPiece(newBoard, self.otherPlayer)
            if myNecro != None and not (self.isPieceNecro(myNecro,newBoard,self.player)):
                myNecro = self.getNecromancerPiece(newBoard, self.player)

            newState = (newBoard,newPlayer,newStepNumber,otherNecro,myNecro)

            # Check if the action have not been played too many times lastly to avoid infinite loop
            # If they have been played too many times they are added at the end of successor list
            # We also change our strategy a bit
            if (self.lastPlayedMoves.count(newAction) > 2 or (stepNumber > 5 and self.lastPlayedMoves[-2] == action) ):
                self.changeStrategy = True
                sortedSuccessors.append((newAction,newState))
            # Here we sort the successor
            else:
                emptyTile = self.getEmptyAround(newBoard,otherNecro)
                if emptyTile > minEmptyTile:
                    sortedSuccessors.insert(insertIndex,(newAction,newState))
                    insertIndex += 1
                elif (emptyTile == 0):
                    sortedSuccessors = []
                    sortedSuccessors.insert(0, (newAction,newState))
                    return sortedSuccessors
                else:
                    minEmptyTile = emptyTile
                    sortedSuccessors.insert(0, (newAction,newState))

        return sortedSuccessors


    def cutoff(self, state, depth):
        """The cutoff function returns true if the alpha-beta/minimax
        search has to stop; false otherwise.
        """

        board = state[0]
        maxDepth = 2
        myNecro = self.getNecromancerPiece(board, self.player)
        otherNecro = self.getNecromancerPiece(board,  self.otherPlayer )

        pieceAroundOtherNecro = 0
        pieceAroundNecro = 0
        if (myNecro != None):
            pieceAroundNecro = len(board.get_non_empty_neighbours(myNecro))
        if (otherNecro != None):
            pieceAroundOtherNecro = len(board.get_non_empty_neighbours(otherNecro))

        # If we are loosing or almost winning lets go a bit deeper in the tree to
        # either save ourself or crush the opponent
        if (pieceAroundNecro > 4):
            maxDepth = 3

        if (self.time_left != None and self.time_left > 40 and pieceAroundOtherNecro > 4):
           maxDepth = 3

        # If the time is limited lets go fast !
        if (self.time_left != None and self.time_left < 15):
           maxDepth = 1

        return (depth >= maxDepth or board.is_finished())

    def evaluate(self, state):
        """The evaluate function must return an integer value
        representing the utility function of the board.
        """
        board = state[0]
        player = state[1]
        otherNecro2 = state[3]
        myNecro2 = state[4]
        myNecro = self.getNecromancerPiece(board, self.player)
        otherNecro = self.getNecromancerPiece(board, self.otherPlayer)
        myNecroMoves = 0
        otherNecroMoves = 0
        # So we don't play the necro first
        pieceAroundNecro = -1
        pieceAroundOtherNecro = 0
        mustMove = 1
        totalDistance = 0
        totalOwnPiece = 0
        jumperOnTopMyNecro = 0
        jumperOnTopOtherNecro = 0
        # Mutliplicator
        totalDisMulti = 2
        pieceAroundNecroMulti = 2
        pieceAroundOtherNecroMulti = 3
        totalOwnPieceMulti = 2

        if (myNecro != None):
            myNecroMoves = len(board.get_necromancer_moves(myNecro))
            pieceAroundNecro = len(board.get_non_empty_neighbours(myNecro))
            if (self.isPieceNecroHugged(board,self.player)):
                jumperOnTopMyNecro = 10

        if (otherNecro != None):
            otherNecroMoves =  len(board.get_necromancer_moves(otherNecro))
            pieceAroundOtherNecro = len(board.get_non_empty_neighbours(otherNecro))
            pieces = self.getOwnPieceOnBoard(board)
            if (self.isPieceNecroHugged(board,self.otherPlayer)):
                jumperOnTopOtherNecro = 15
            if (pieces != None):
               # Compute the distance between our piece and opponent necromancer
               for p in pieces:
                  totalDistance += self.FindManathanDistance(p,otherNecro)
               totalDistance = totalDistance / len(pieces)
               totalOwnPiece = 11 - self.getUnplacedPieceNumber(board)

        if (pieceAroundOtherNecro > 5):
            pieceAroundOtherNecroMulti = 5
        if (pieceAroundNecro > 4):
            pieceAroundNecroMulti = 5
        if (totalOwnPiece == 11):
            totalOwnPieceMulti = 0
        if (self.changeStrategy or self.time_left != None and self.time_left < 15):
           totalDisMulti = 2

        # Compute the positive and negative thing to evaluate the current state.
        positivePart = (pieceAroundOtherNecroMulti * pieceAroundOtherNecro ) + myNecroMoves + totalOwnPieceMulti * totalOwnPiece 
        negativePart = (pieceAroundNecroMulti * pieceAroundNecro) + otherNecroMoves + (totalDisMulti * totalDistance) + jumperOnTopMyNecro
        return positivePart - negativePart

    def play(self, board, player, step, time_left):
        """This function is used to play a move according
        to the board, player and time left provided as input.
        It must return an action representing the move the player
        will perform.
        """
        self.player = player
        self.time_left = time_left
        if (self.player == zombies.PLAYER1):
            self.otherPlayer = zombies.PLAYER2
        else:
            self.otherPlayer = zombies.PLAYER1

        state = (board, player, step)
        action = minimax.search(state, self)
        self.changeStrategy = False
        # Store the last 20 actions so we can analyse them
        self.lastPlayedMoves.append(action)
        if (len(self.lastPlayedMoves) > 40):
            self.lastPlayedMoves.pop(0)
        return action

    def getNecromancerPiece(self, board, player):
        if board.unplaced_pieces[NECROMANCER * player] == 0:
            for piece in board.pieces:
                if (type(board.pieces[piece]) is int and board.pieces[piece] * player == NECROMANCER) or \
                   (type(board.pieces[piece]) is list and NECROMANCER * player in board.pieces[piece]):
                      return piece

    def isPieceNecroHugged(self,board,player):
        if board.unplaced_pieces[NECROMANCER * player] == 0:
            for piece in board.pieces:
                if (type(board.pieces[piece]) is list and NECROMANCER * player in board.pieces[piece]):
                    return True
        return False

    def isPieceNecro(self,piece,board,player):
        if board.unplaced_pieces[NECROMANCER * player] == 0:
            if (type(board.pieces[piece]) is int and board.pieces[piece] * player == NECROMANCER) or \
               (type(board.pieces[piece]) is list and NECROMANCER * player in board.pieces[piece]):
                return True
        return False

    def getEmptyAround(self,board,piece):
        if (piece == None):
            return 6
        return 6 - len(board.get_non_empty_neighbours(piece))

    def reverse_insort(self,a, x, lo=0, hi=None):
        if lo < 0:
            raise ValueError('lo must be non-negative')
        if hi is None:
            hi = len(a)
        while lo < hi:
            mid = (lo+hi)//2
            if x > a[mid]: hi = mid
            else: lo = mid+1
        a.insert(lo, x)

    def getOwnPieceOnBoard(self,board):
        ownPieces = []
        for piece in board.pieces:
            if (type(board.pieces[piece]) is int and board.pieces[piece] * self.player > 0) or \
               (type(board.pieces[piece]) is list and (NECROMANCER * self.player in board.pieces[piece] \
                or HUGGER * self.player in board.pieces[piece] or SPRINTER * self.player in board.pieces[piece] \
                or JUMPER * self.player in board.pieces[piece] or CREEPER * self.player in board.pieces[piece])):
                  ownPieces.append(piece)
        return ownPieces

    def getUnplacedPieceNumber(self,board):
        c = 0 
        c += board.unplaced_pieces[NECROMANCER * self.player]
        c += board.unplaced_pieces[HUGGER * self.player]
        c += board.unplaced_pieces[JUMPER * self.player]
        c += board.unplaced_pieces[CREEPER * self.player]
        c += board.unplaced_pieces[SPRINTER * self.player] 
        return c

    def FindManathanDistance(self, p1, p2):
        xdiff = abs(p1[0] - p2[0])
        ydiff = abs(p1[1] - p2[1])
        return xdiff + ydiff

if __name__ == "__main__":
    zombies.agent_main(Agent())
