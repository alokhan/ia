from search import *
import time
from math import *
from operator import itemgetter, attrgetter, methodcaller
from copy import deepcopy

class Item():
    def __init__(self,name,weight,utility):
        self.name = name
        self.weight = int(weight)
        self.utility = utility

    def __eq__(self,other):
        if isinstance(other, self.__class__):
            return self.weight == other.weight
        else:
            return False

    def __lt__(self,other):
        return self.weight < other.weight

    def __gt__(self,other):
        return self.weight > other.weight

class Knapsack(Problem):

    def __init__(self,init):
	# Open file with path given as param
        f = open(init, 'r')
	# Read the each line of the file into a list
        lines = f.readlines()
        f.close()

        self.number_of_item_available = int(lines[0])
        self.available_items = set()
        for i in range(1, len(lines)-1):
            line = lines[i].split()
            self.available_items.add((int(line[0]),int(line[1]),int(line[2])))

        self.capacity = int(lines[-1])

        sorted_available_item = sorted(self.available_items, key=itemgetter(1))

        inventory = set()
        for item in sorted_available_item:
            if self.get_weight(inventory) + item[1] <= self.capacity:
                inventory.add(item)
                self.available_items.remove(item)

        self.best_state = inventory
        state = (inventory,set(self.available_items))
        super().__init__(state)

    def get_weight(self,item_list):
        w = 0
        for item in item_list:
            w += item[1]
        return w

    def value(self,state):
        return self.get_weight(state[0])

    def successor(self, state):
        available_items = state[1]
        current_inventory = state[0]
        for item in available_items:
            if self.get_weight(current_inventory) + item[1] <= self.capacity:
                inventory_copy = set(current_inventory)
                available_items_copy = set(available_items)
                inventory_copy.add(item)
                available_items_copy.remove(item)
                yield ("add", (inventory_copy,available_items_copy))

            for my_item in current_inventory:
                if self.get_weight(current_inventory) - my_item[1] + item[1] <= self.capacity:
                    inventory_copy = set(current_inventory)
                    available_items_copy = set(available_items)
                    inventory_copy.remove(my_item)
                    available_items_copy.add(my_item)
                    inventory_copy.add(item)
                    available_items_copy.remove(item)
                    yield ("rep",(inventory_copy,available_items_copy))

        for my_item in current_inventory:
                inventory_copy = set(current_inventory)
                inventory_copy.remove(my_item)
                available_items_copy = set(available_items)
                available_items_copy.add(my_item)
                yield ("del",(inventory_copy,available_items_copy))



def maxvalue(problem, limit=100,callback=None):
    current = LSNode(problem, problem.initial, 0)
    best = current
    for step in range(limit):
        if callback is not None:
            callback(current)
        # select best node from the expand list
        best_from_list = list(current.expand())[0]
        for node in list(current.expand()):
            if node.value() > best_from_list.value():
                best_from_list = node

        current = best_from_list
        if current.value() > best.value():
            best = current
    return best

def randomized_maxvalue(problem, limit=100,callback=None):
    current = LSNode(problem, problem.initial, 0)
    best = current
    for step in range(limit):
        if callback is not None:
            callback(current)
        list_ex = list(current.expand())
        list_ex.sort(key=lambda item: item.value(), reverse=True)
        list_ex = list_ex[0:5]
        current = random.choice(list_ex)
        if current.value() > best.value():
            best = current
    return best

###############

maxvalue_best = 0
randomized_maxvalue_best = 0
random_walk_best = 0

maxvalue_avg_step = 0
randomized_maxvalue_avg_step = 0
random_walk_avg_step = 0

maxvalue_time_avg = 0
randomized_time_avg = 0
random_walk_time_avg = 0

for i in range(0,10):
    problem=Knapsack(sys.argv[1])
    start_time = time.time()
    node = randomized_maxvalue(problem)
    time_total = time.time() - start_time
    randomized_time_avg += time_total
    #example of print
    if (node.value() > randomized_maxvalue_best):
        randomized_maxvalue_best = node.value()
    randomized_maxvalue_avg_step += node.step

    start_time = time.time()
    node = maxvalue(problem)
    time_total = time.time() - start_time
    maxvalue_time_avg += time_total
    #example of print
    if (node.value() > maxvalue_best):
        maxvalue_best = node.value()
    maxvalue_avg_step += node.step

    start_time = time.time()
    node = random_walk(problem)
    time_total = time.time() - start_time
    random_walk_time_avg += time_total
    #example of print
    if (node.value() > random_walk_best):
        random_walk_best = node.value()
    random_walk_avg_step += node.step

randomized_maxvalue_avg_step = randomized_maxvalue_avg_step/10
maxvalue_avg_step = maxvalue_avg_step/10
random_walk_avg_step = random_walk_avg_step/10

randomized_time_avg = randomized_time_avg/10
maxvalue_time_avg = maxvalue_time_avg/10
random_walk_time_avg = random_walk_time_avg/10

print("------- average randomizer maxvalue -------")
print("best value " + str(randomized_maxvalue_best))
print("average step " + str(randomized_maxvalue_avg_step))
print("average time " + str(randomized_time_avg))

print("-------- average maxvalue -----------")
print("best value " + str(maxvalue_best))
print("average step " + str(maxvalue_avg_step))
print("average time " + str(maxvalue_time_avg))

print("-------- average random walk -----------")
print("best value " + str(random_walk_best))
print("average step " + str(random_walk_avg_step))
print("average time " + str(random_walk_time_avg))
