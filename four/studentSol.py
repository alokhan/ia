#!/usr/bin/env python3

import rpg

def get_clauses(merchant, level):
	# Append all clauses needed to find the correct equipment in the 'clauses' list.
	#
	# Minisat variables are represented with integers. As such you should use
	# the index attribute of classes Ability and Equipment from the rpg.py module
	#
	# The equipments and abilities they provide read from the merchant file you passed
	# as argument are contained in the variable 'merchant'.
	# The enemies and abilities they require to be defeated read from the level file you
	# passed as argument are contained in the variable 'level'
	#
	# For example if you want to add the clauses equ1 or equ2 or ... or equN (i.e. a
	# disjunction of all the equipment pieces the merchant proposes), you should write:
	#
	# clauses.append(tuple(equ.index for equ in merchant.equipments))
	clauses = []

    # Provides
	for equ in merchant.equipments:
		for abi in equ.provides:
			clauses.append((equ.index*-1,abi.index))

	# is provided
	all_equipment_needed = set()
	for abi_name in level.ability_names:
		needed_equipment = merchant.abi_map[abi_name].provided_by
		all_equipment_needed = all_equipment_needed | needed_equipment
		equ_list = []
		for equ in needed_equipment:
			equ_list.append(equ.index)

		clauses.append(tuple(equ_list))

    #conflicts
	conflicts = set()
	for equ in merchant.equipments:
		t = []
		t.append(equ.index*-1)
		t.append(equ.conflicts.index*-1)
		conflicts.add(tuple(t))

	for c in conflicts:
		clauses.append(c)

    # requires
	for req in level.ability_names:
		clauses.append((merchant.abi_map[req].index,))

	return clauses

def get_nb_vars(merchant, level):
	# nb_vars should be the number of different variables present in your list 'clauses'
	#
	# For example, if your clauses contain all the equipments proposed by merchant and
	# all the abilities provided by these equipment, you would have:
	# nb_vars = len(merchant.abilities) + len(merchant.equipments)

	nb_vars = len(merchant.abilities) + len(merchant.equipments)
	return nb_vars
