'''NAMES OF THE AUTHOR(S): Florent Klein and Alois Paulus'''

from search import *
import time

######################  Implement the search #######################

# Class representing a tile which contains multiple letters.
class MergedTile:

    def __init__(self):
        self.tiles = []
    def add(self,tile):
        if (tile[0] == "["):
           tile = tile[1:-1].split(",")
           self.tiles = self.tiles + tile
        else:
           self.tiles.append(tile)

    def __str__(self):
        line="["
        for i in range(0,len(self.tiles)):
            line += str(self.tiles[i])
            if (i != len(self.tiles) - 1):
                line += ","
        line = line + "]"
        return line

# The class state with str redefined for specific output format
# eq and hash redifined to considere symmetrical states as same states
class State:

    def __init__(self,board):
        self.board = board
    def __str__(self):
        output=""
        for i in range(0,len(self.board)):
            line=""
            for u in range(0,len(self.board[i])):
                if (u < len(self.board[i]) - 1):
                    line=line+self.board[i][u] + " "
                else:
                    line=line+self.board[i][u]
            output= output + line + "\n"
        return output

    def __eq__(self,other):
        return hash(self) == hash(other)

    def __hash__(self):
        tupleToHash = ()
        for i in range(0,len(self.board)):
            for u in range(0,len(self.board[i])):
                if(self.board[i][u] != "."):
                    tupleToHash = tupleToHash + (i , u) 
        #print(tupleToHash)
        return hash(tupleToHash)             
  

class Koutack(Problem):

    def __init__(self,init):
        self.numberNodeExplored = 0
	# Open file with path given as param
        f = open(init, 'r')
	# Read the each line of the file into a list
        lines = f.readlines()
        f.close()
        initBoard = ()
	# Construct a tuple of tuples for the state
        length = len(lines)
        width = len(lines[0])
        for i in range(len(lines) -1, -1 , -1):
            initBoard = (tuple(lines[i].split()),) + initBoard
        initState = State(initBoard)
        super().__init__(initState)

    # Check if only on tile is not a dot
    def goal_test(self, state):
        count = 0;
        self.numberNodeExplored += 1
        for i in range(0,len(state.board)):
            count = count + state.board[i].count('.')
        return (count == ((len(state.board[i]) * len(state.board)) - 1))

        
    def successor(self, state):
        for i in range(0, len(state.board)):
            for u in range (0, len(state.board[i])):
                # Check if we can merge any neighbours when clicking on current position
                if (state.board[i][u] == ".") and self.clickable(i,u,state):
                     yield [(i,u),self.getNextState(i,u,state)]

    # Count the number of neighbours for the current position	
    def clickable(self,i,u,state):
        counter = 0
        if (i > 0) :
           if (state.board[i-1][u] != "."):
               counter = counter +1
        if (u + 1 < len(state.board[i])):
           if (state.board[i][u+1] != "."):
               counter = counter +1
        if (i + 1 < len(state.board)):
           if (state.board[i+1][u] != "."):
               counter = counter +1
        if (u > 0):
           if (state.board[i][u-1] != "."):
               counter = counter +1
        return counter > 1
           
    # Merge all the neighbours from the current position to a tile 
    # and return a new state
    def getNextState(self,i,u,state):

        mergedTile = MergedTile()
        newStateList = []
        for tup in state.board:
            newStateList.append(list(tup))

        if (i > 0) :
           if (state.board[i-1][u] != "."):
               mergedTile.add(state.board[i-1][u])
               newStateList[i-1][u] = "."
        if (u + 1 < len(state.board[i])):
           if (state.board[i][u+1] != "."):
               mergedTile.add(state.board[i][u+1])
               newStateList[i][u+1] = "."
        if (i + 1 < len(state.board)):
           if (state.board[i+1][u] != "."):
               mergedTile.add(state.board[i+1][u])
               newStateList[i+1][u] = "."
        if (u > 0):
           if (state.board[i][u-1] != "."):
               mergedTile.add(state.board[i][u-1])
               newStateList[i][u-1] = "."

        newStateList[i][u] = str(mergedTile)
        newStateTuple = ()
        for i in range(len(newStateList)-1, -1 , -1):
            newStateTuple = (tuple(newStateList[i]),) + newStateTuple
        newState = State(newStateTuple)
        return newState


###################### Launch the search #########################
startTime = time.time()
problem=Koutack(sys.argv[1])
#example of bfs search
node=breadth_first_graph_search(problem)
#node=depth_first_graph_search(problem)
#node=breadth_first_tree_search(problem)
#node=depth_first_tree_search(problem)
#example of print
path=node.path()
path.reverse()
for n in path:
    print(n.state) #assuming that the __str__ function of states output the correct format

#Print function for question 5
#print(sys.argv[1].split("/")[2] + " & " + str(time.time() - startTime) + " & "+ str(problem.numberNodeExplored) + " & " + str(len(path)) + " \\\\ \hline ")
        
